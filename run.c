#include <stdio.h>
#include "sudoku.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[]){
	Grid_T table,g;
	int a;
	if(argc==1){
		table =sudoku_read();
		fprintf(stderr,"\x1b[33m" "\n" "\x1b[0m");
		sudoku_print(stderr,table);
		grid_set_unique(&table);
		table=sudoku_solve(table);
		if(sudoku_is_correct(table)){
			if(sudoku_solution_is_unique(table)){
				fprintf(stderr,"\x1b[33m" "Puzzle has a (unique choice) solution:\n" "\x1b[0m");
			}else{
				fprintf(stderr,"\x1b[33m" "Puzzle has more than one choice solution:\n" "\x1b[0m");
			}
		}else{
			fprintf(stderr,"\x1b[33m" "Puzzle has not a solution:\n" "\x1b[0m");
		}
		sudoku_print(stderr,table);
	}else if(argc==2 && !(strcmp(argv[1], "-c"))){
		table =sudoku_read();
                fprintf(stderr,"\x1b[33m" "\n" "\x1b[0m");
		sudoku_print(stderr,table);
		if(sudoku_is_correct(table)){
			fprintf(stderr,"\x1b[33m" "Puzzle is correct:\n" "\x1b[0m");
		}else{
			fprintf(stderr,"\x1b[33m" "Puzzle is not correct.\n" "\x1b[0m");
			sudoku_print_errors(table);
		}
	}else if(argc==3 && !(strcmp(argv[1], "-g")) && atoi(argv[2])>=0 && atoi(argv[2])<=81){
		table=sudoku_generate(atoi(argv[2]));
		fprintf(stderr,"\x1b[33m" "New" "\x1b[0m");
                grid_set_unique(&table);
                g=sudoku_solve(table);
                if(sudoku_solution_is_unique(g)){
		fprintf(stderr,"\x1b[33m" " (unique choice solvable)" "\x1b[0m");
		}
		fprintf(stderr,"\x1b[33m" " puzzle:\n" "\x1b[0m");
		sudoku_print(stderr,table);
	}else if(argc==4 && !(strcmp(argv[1], "-g")) && atoi(argv[2])>=0 && atoi(argv[2])<=81 && !(strcmp(argv[3], "-u"))){
		fprintf(stderr,"\x1b[33m" "New (unique choice solvable) puzzle:\n" "\x1b[0m");
		a=atoi(argv[2]);
		while(a<81){
			table=sudoku_generate(a);
			grid_set_unique(&table);
			g=sudoku_solve(table);
			if(sudoku_solution_is_unique(g)) break;
			a++;
		}
		sudoku_print(stderr,table);
        }else{
		fprintf(stderr,"\x1b[33m" "Oops! Value or number of the arguments is not corrent. \nPlease try again...\n" "\x1b[0m");
	}
	return 0;
}
