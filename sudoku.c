#include <stdio.h>
#include "sudoku.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/* Reads a sudoku puzzle from stdin.*/
Grid_T sudoku_read(void){
	int i,j;
	char a;
	Grid_T table;
	for(i=0; i<9; i++){

		for(j=0; j<9; j++){
			a=getchar()-48;
			grid_update_value(&table, i, j, a);
			grid_clear_count(&table,i,j);
			getchar(); /*for space and \n*/
		}
	}
	return table;
}

/* Print the sudoku puzzle defined by g to stream s in the same format
   as expected by sudoku_read(). */
void sudoku_print(FILE *s, Grid_T g){
	int i,j;
	for(i=0; i<9; i++){

		for(j=0; j<9; j++){

			fprintf(s,"%d ",grid_read_value(g, i, j));
		}
		fprintf(s,"\n");
	}
}

/* Print all row, col, sub-grid errors/conflicts found in puzzle g;
   some errors may be reported more than once. */
void sudoku_print_errors(Grid_T g){
		int i,j,n;
		for(i=0; i<9; i++){
			for(j=0; j<9; j++){
				n=grid_read_value(g,i,j);
				if(n==0){
					fprintf(stderr,"Cell (%d,%d) is empty\n",i,j);
				}else if(check_subgrid(g,i,j,n)==2){
					check_subgrid_error(g,i,j,n);
				}else if(check_grid_row(g,i,n)==2){
					check_row_error(g,i,n);
				}else if(check_grid_column(g,j,n)==2){
					check_column_error(g,j,n);
                                }
			}
		}
	return;
}

/* Print all row errors and conflicts found in puzzle g*/
void check_row_error(Grid_T g,int row, int n){
	int i,a;
        int A[9];
        a=0;
        fprintf(stderr,"In row %d cells",row);
        for(i=0; i<9; i++){
                if(grid_read_value(g,row,i)==n){
                        A[a]=i+1;
			a++;
                }
        }
        i=1;
        fprintf(stderr," %d",A[0]);
        while(i<a-1){
                fprintf(stderr,", %d",A[i]);
                i++;
        }
        fprintf(stderr," and %d are the same\n",A[a-1]);
	return;
}

/* Print all column errors and conflicts found in puzzle g*/
void check_column_error(Grid_T g,int col, int n){
        int i,a;
	int A[9];
	a=0;
	fprintf(stderr,"In column %d cells",col);
        for(i=0; i<9; i++){
		if(grid_read_value(g,i,col)==n){
			A[a]=i;
			a++;
                }
        }
	i=1;
	fprintf(stderr," %d",A[0]);
	while(i<=a-2){
		fprintf(stderr,", %d",A[i]);
		i++;
	}
	fprintf(stderr," and %d are the same\n",A[a-1]);
	return;
}

/* Print all sub-grid  errors and conflicts found in puzzle g*/
void check_subgrid_error(Grid_T g,int row,int column,int n){
        int a,b,i,j,l;
	int A[9];
        if(row<3 && row>=0){
                a=0;
        }else if(row>=3 && row<6){
                a=3;
        }else{
                a=6;
        }

        if(column<3 && column>=0){
                b=0;
        }else if(column>=3 && column<6){
                b=3;
        }else{
                b=6;
        }
	fprintf(stderr,"In block %d cells",a+b/3+1);
	l=0;
        for(i=a; i<a+3; i++){
                for(j=b; j<b+3; j++){
                        if(grid_read_value(g,i,j)==n){
				A[l]=3*(j-b)+i-a+1;
				l++;
                        }
                }
        }
	i=1;
        fprintf(stderr," %d",A[0]);
        while(i<=l-2){
                fprintf(stderr,", %d",A[i]);
                i++;
        }
        fprintf(stderr," and %d are the same\n",A[l-1]);

        return;
}

/* Return true if puzzle g is correct. */
int sudoku_is_correct(Grid_T g){
	int i,j;
	for(i=0; i<9; i++){
		for(j=0; j<9; j++){
			if(grid_read_value(g,i,j)==0){
				return 0;
			}
		}
	}
	return 1;
}

/* In case of fail of a number's choice at solving process
it puts another valid number in the element's position. */
void sudoku_choose_other_choice(Grid_T *g, int row, int col,int *n){
	srand(getpid());
	while(!(grid_choice_is_valid(*g,row,col,*n))){
		*n = rand()%9+1;
	}
	sudoku_update_choice(g,row,col,*n);
        return;
	/*for(*n=1; *n<10; (*n)++){
        	if(grid_choice_is_valid(*g,row,col,*n)){
        		sudoku_update_choice(g,row,col,*n);
                	break;
		}
	}*/
	return;
}
/* Sovle puzzle g and return the solved puzzle; if the puzzle has
   multiple solutions, return one of the possible solutions. */
Grid_T sudoku_solve(Grid_T g){
	int n;
	int row, col, count=0;
	Grid_T temp;
	sudoku_init_choices(&g);
	count=sudoku_try_next(g,&row, &col, count);
	if(count==0){
		return g;
	}else if(count==1){
		for(n=1; n<10; n++){
			if(grid_choice_is_valid(g,row,col,n)){
				sudoku_update_choice(&g,row,col,n);
				break;
			}
		}
   		if(!(sudoku_is_correct(g))){
			return sudoku_solve(g);
		}
	}else if(count>1){
		grid_clear_unique(&g);
		while(!(sudoku_is_correct(g))&& count>=1){
			sudoku_copy(&temp, g);
			sudoku_choose_other_choice(&temp,row,col,&n);
			temp=sudoku_solve(temp);
			if(sudoku_is_correct(temp)) return temp;
			grid_clear_choice(&g,row,col,n);
			count=count-1;
		}
	}
	return g;
}
/* Returns true if solution g, as returned by sudoku_solve, has a
   unique choice for each step (no backtracking required). (Note, g
   must have been computed with the use of sudoku_solve.) */
int sudoku_solution_is_unique(Grid_T g){
	return grid_read_unique(g);
}

/* Returns a sudoku table with only 0-values.*/
Grid_T sudoku_initialize(){
	Grid_T g;
	int i,j;
	for(i=0; i<9; i++){
		for(j=0; j<9; j++){
			grid_update_value(&g,i,j,0);
		}
	}
	return g;
}
/* Generate and return a sudoku puzzle with "approximately" nelts
   elements having non-0 value. The smaller nelts the harder may be to
   generate/solve the puzzle. For instance, nelts=81 should return a
   completed and correct puzzle. */
Grid_T sudoku_generate(int nelts){
	int i,row,col;
	Grid_T g;
	g=sudoku_initialize();
	g=sudoku_solve(g);
	srand(getpid());
	for(i=0; i<81-nelts; i++){
		do{
			row=rand()%9;
			col=rand()%9;
		}while(grid_read_value(g,row,col)==0);
		grid_update_value(&g,row,col,0);
	}
	return g;
}

/* It takes a grid and returns by reference the row and column of the element 
with least valid choices and by value the the number of this choices.*/
int sudoku_try_next(Grid_T g, int *row, int *col, int count){
	int j,i;
	for(i=0; i<9; i++){
		for(j=0; j<9; j++){
			if(grid_read_value(g,i,j)==0 && grid_read_count(g,i,j)==count){
				*row=i;
				*col=j;
				return count;
			}
        	}
	}
	if(count<9){
		count=count+1;
		return sudoku_try_next(g,  row,  col, count);
	}
	return 0;
}

/* It puts the choosen number n to the position (i,j).*/
int sudoku_update_choice(Grid_T *g, int i, int j, int n){
	grid_update_value(g,i,j,n);
	grid_clear_choice(g,i,j,n);
	grid_sub_count(g, i, j);
	return 0;
}

/* Figure out the coices of numbers are for every element. */
void sudoku_init_choices(Grid_T *g){
        int i,j,n;
	for(i=0; i<9; i++){

                for(j=0; j<9; j++){

                        if(grid_read_value(*g,i,j)==0){
				grid_clear_count(g,i,j);
                                for(n=1; n<10; n++){
					g->elts[i][j].choices.num[n]=0;
                                        if(!(check_grid_row(*g,i,n)) &&
                                        !(check_grid_column(*g,j,n)) &&
                                        !(check_subgrid(*g,i,j,n))){
                                                grid_set_choice(g,i,j,n);
                                        }
                                }
			}else{
				grid_clear_count(g,i,j);
			}
                }
        }
}

/* Check for the given row if the values
   of the elements match to the given n. */
int check_grid_row(Grid_T g,int row, int n){
	int i,c;
	c=0;
	for(i=0; i<9; i++){
		if(grid_read_value(g,row,i)!=0&&grid_read_value(g,row,i)==n){
			c++;
		}
	}
	return c;
}

/* Check for the given column if the values
   of the elements match to the given n. */
int check_grid_column(Grid_T g, int column, int n){
	int i,c;
	c=0;
	for(i=0; i<9; i++){
                if(grid_read_value(g,i,column)!=0 && grid_read_value(g,i,column)==n){
                        c++;
                }
        }
        return c;

}

/* Check for the subgrid that contains the
given element of the value matches the given n.*/
int check_subgrid(Grid_T g,int row,int column,int n){
	int a,b,i,j,c;
	c=0;
	if(row<3 && row>=0){
		a=0;
	}else if(row>=3 && row<6){
		a=3;
	}else{
		a=6;
	}

	if(column<3 && column>=0){
                b=0;
        }else if(column>=3 && column<6){
                b=3;
        }else{
                b=6;
        }

	for(i=a; i<a+3; i++){
		for(j=b; j<b+3; j++){
			if(grid_read_value(g,i,j)!=0 && grid_read_value(g,i,j)==n){
				c++;
                	}
        	}
	}
	return c;
}

/* Copy a Grid_T element (table) to another g.*/
void sudoku_copy(Grid_T *g, Grid_T table){
	int i,j,n;
	g->unique=table.unique;
	for(i=0; i<9; i++){
		for(j=0; j<9; j++){
			g->elts[i][j].val=table.elts[i][j].val;
			g->elts[i][j].choices.count=table.elts[i][j].choices.count;
			for(n=0; n<10; n++){
				g->elts[i][j].choices.num[n]=table.elts[i][j].choices.num[n];
			}
		}
	}
	return;
}

int main(int argc, char *argv[]){
	Grid_T table,g;
	int a;
	if(argc==1){
		table =sudoku_read();
		fprintf(stderr,"\x1b[33m" "\n" "\x1b[0m");
		sudoku_print(stderr,table);
		grid_set_unique(&table);
		table=sudoku_solve(table);
		if(sudoku_is_correct(table)){
			if(sudoku_solution_is_unique(table)){
				fprintf(stderr,"\x1b[33m" "Puzzle has a (unique choice) solution:\n" "\x1b[0m");
			}else{
				fprintf(stderr,"\x1b[33m" "Puzzle has more than one choice solution:\n" "\x1b[0m");
			}
		}else{
			fprintf(stderr,"\x1b[33m" "Puzzle has not a solution:\n" "\x1b[0m");
		}
		sudoku_print(stderr,table);
	}else if(argc==2 && !(strcmp(argv[1], "-c"))){
		table =sudoku_read();
                fprintf(stderr,"\x1b[33m" "\n" "\x1b[0m");
		sudoku_print(stderr,table);
		if(sudoku_is_correct(table)){
			fprintf(stderr,"\x1b[33m" "Puzzle is correct:\n" "\x1b[0m");
		}else{
			fprintf(stderr,"\x1b[33m" "Puzzle is not correct.\n" "\x1b[0m");
			sudoku_print_errors(table);
		}
	}else if(argc==3 && !(strcmp(argv[1], "-g")) && atoi(argv[2])>=0 && atoi(argv[2])<=81){
		table=sudoku_generate(atoi(argv[2]));
		fprintf(stderr,"\x1b[33m" "New" "\x1b[0m");
                grid_set_unique(&table);
                g=sudoku_solve(table);
                if(sudoku_solution_is_unique(g)){
		fprintf(stderr,"\x1b[33m" " (unique choice solvable)" "\x1b[0m");
		}
		fprintf(stderr,"\x1b[33m" " puzzle:\n" "\x1b[0m");
		sudoku_print(stderr,table);
	}else if(argc==4 && !(strcmp(argv[1], "-g")) && atoi(argv[2])>=0 && atoi(argv[2])<=81 && !(strcmp(argv[3], "-u"))){
		fprintf(stderr,"\x1b[33m" "New (unique choice solvable) puzzle:\n" "\x1b[0m");
		a=atoi(argv[2]);
		while(a<81){
			table=sudoku_generate(a);
			grid_set_unique(&table);
			g=sudoku_solve(table);
			if(sudoku_solution_is_unique(g)) break;
			a++;
		}
		sudoku_print(stderr,table);
        }else{
		fprintf(stderr,"\x1b[33m" "Oops! Value or number of the arguments is not corrent. \nPlease try again...\n" "\x1b[0m");
	}
	return 0;
}
