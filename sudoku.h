/********************************************************************
   sudoku.h

   Sudoku solver/generator interface.
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "grid.h"

/* Read a sudoku grid from stdin and return an object Grid_T
   initialized to these values. The input has the format:

1 2 3 4 5 6 7 8 9
4 5 6 7 8 9 1 2 3
7 8 9 1 2 3 4 5 6
2 3 4 5 6 7 8 9 1
5 6 7 8 9 1 2 3 4
8 9 1 2 3 4 5 6 7
3 4 5 6 7 8 9 1 2
6 7 8 9 1 2 3 4 5
9 1 2 3 4 5 6 7 8

   Each number is followed by a space. Each line is terminated with
   \n. Values of 0 indicate empty grid cells.
*/
Grid_T sudoku_read(void);

/* Print the sudoku puzzle defined by g to stream s in the same format
   as expected by sudoku_read(). */
void sudoku_print(FILE *s, Grid_T g);

/* Print all row, col, sub-grid errors/conflicts found in puzzle g;
   some errors may be reported more than once. */
void sudoku_print_errors(Grid_T g);

/* Print all column errors and conflicts found in puzzle g*/
void check_column_error(Grid_T g,int col, int n);

/* Print all sub-grid  errors and conflicts found in puzzle g*/
void check_subgrid_error(Grid_T g,int row,int column,int n);

/* Print all row errors and conflicts found in puzzle g*/
void check_row_error(Grid_T g,int row, int n);

/* Return true if puzzle g is correct. */
int sudoku_is_correct(Grid_T g);

/* In case of fail of a number's choice at solving process
it puts another valid number in the element's position. */
void sudoku_choose_other_choice(Grid_T *g, int row, int col,int *n);

/* Check for the given row if the values
   of the elements match to the given n. */
int check_grid_row(Grid_T g,int row,int n);

/* Check for the given column if the values
   of the elements match to the given n. */
int check_grid_column(Grid_T g, int column ,int n);

/* Check for the subgrid that contains the
given element of the value matches the given n.*/
int check_subgrid(Grid_T g,int i,int j,int n);

/* Copy a Grid_T element (table) to another g.*/
void sudoku_copy(Grid_T *g, Grid_T table);
/* It takes a grid and returns by reference the row and column of the element
with least valid choices and by value the the number of this choices.*/
int sudoku_try_next(Grid_T g, int *row, int *col, int count);

/* It puts the choosen number n to the position (i,j).*/
int sudoku_update_choice(Grid_T *g, int i, int j, int n);

/* Figure out the coices of numbers are for every element. */
void sudoku_init_choices(Grid_T *g);

/* Sovle puzzle g and return the solved puzzle; if the puzzle has
   multiple solutions, return one of the possible solutions. */
Grid_T sudoku_solve(Grid_T g);

/* Returns true if solution g, as returned by sudoku_solve, has a
   unique choice for each step (no backtracking required). (Note, g
   must have been computed with the use of sudoku_solve.) */
int sudoku_solution_is_unique(Grid_T g);

/* Returns a sudoku table with only 0-values.*/
Grid_T sudoku_initialize();

/* Generate and return a sudoku puzzle with "approximately" nelts
   elements having non-0 value. The smaller nelts the harder may be to
   generate/solve the puzzle. For instance, nelts=81 should return a
   completed and correct puzzle. */
Grid_T sudoku_generate(int nelts);


